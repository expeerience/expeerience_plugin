module ExpeeriencePlugin
  class Engine < ::Rails::Engine
    engine_name "ExpeeriencePlugin".freeze
    isolate_namespace ExpeeriencePlugin

    config.after_initialize do
      Discourse::Application.routes.append do
        mount ::ExpeeriencePlugin::Engine, at: "/expeerience-plugin"
      end
    end
  end
end
