module ExpeeriencePlugin
  class ExpeeriencePluginController < ::ApplicationController
    requires_plugin ExpeeriencePlugin

    before_action :ensure_logged_in

    def index
    end
  end
end
