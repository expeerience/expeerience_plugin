require_dependency "expeerience_plugin_constraint"

ExpeeriencePlugin::Engine.routes.draw do
  get "/" => "expeerience_plugin#index", constraints: ExpeeriencePluginConstraint.new
  get "/actions" => "actions#index", constraints: ExpeeriencePluginConstraint.new
  get "/actions/:id" => "actions#show", constraints: ExpeeriencePluginConstraint.new
end
