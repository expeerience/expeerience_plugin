export default function() {
  this.route("expeerience-plugin", function() {
    this.route("actions", function() {
      this.route("show", { path: "/:id" });
    });
  });
};
