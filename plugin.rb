# frozen_string_literal: true

# name: expeerience_plugin
# about: expeerience
# version: 0.2
# authors: 
# url: https://github.com/

register_locale("en_INT", name: "English (International)", nativeName: "English (International)", fallbackLocale: "en")

register_asset 'stylesheets/common/expeerience-plugin.scss'
register_asset 'stylesheets/desktop/expeerience-plugin.scss', :desktop
register_asset 'stylesheets/mobile/expeerience-plugin.scss', :mobile
register_asset "public/images/emails/thumbs-up.png", :server_side

enabled_site_setting :expeerience_enabled


PLUGIN_NAME ||= 'ExpeeriencePlugin'

load File.expand_path('lib/expeerience-plugin/engine.rb', __dir__)


after_initialize do
  # https://github.com/discourse/discourse/blob/master/lib/plugin/instance.rb
  
  # Should only work for PRIVATE_WORKSPACE category archetype
  ::ActionMailer::Base.prepend_view_path File.expand_path("../custom_views", __FILE__)

  DiscourseEvent.on(:category_created) do |category|
    puts "@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@ "
    puts "@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@ "
    puts "@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@ "
    puts "@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@ "
    # change owner
    owner = User.find_by(username: category.custom_fields["owner"])
    if owner
      category.update(user: owner)
    end
    puts "@@@@@@@@@@@@@@@@@@@@@@@ 1"
    if "private" == category.custom_fields["archetype"]
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 2"
      update_welcome_topic_owner(category)
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 3"
      create_about_topic(category)
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 4"
      create_groups(category)
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 5"
    end

    puts "@@@@@@@@@@@@@@@@@@@@@@@ 6"
    if "publicparent" == category.custom_fields["archetype"]
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 7"
      # if public parent, add Everyone to groups
      category.groups << Group.find(0)
      # category.groups.delete(Group.find(1))
      category.update(read_restricted: false)
    end

    if "simple" == category.custom_fields["archetype"]
      puts "@@@@@@@@@@@@@@@@@@@@@@@ 7.5"
      # if simple, add Everyone to groups, remove Admins
      category.groups << Group.find(0)
      category.groups.delete(Group.find(1))
      category.update(read_restricted: false)
    end

    if "publicparent" == category.custom_fields["archetype"] || "privateparent" == category.custom_fields["archetype"]
      category.update(
        subcategory_list_style: 'boxes_with_featured_topics',
        show_subcategory_list: true)
    end


    # TODO change the name to include the category ID
    if !category.name.end_with?(" (##{category.id})") && "private" == category.custom_fields["archetype"]
      category.update(name: category.name+" (##{category.id})")
    end
  end

  # Add child group to parents security groups, if a child changes its parent
  DiscourseEvent.on(:category_updated) do |category|
    puts ">>>>>>>>> EDITING #{category.name}"
    add_groups_to_parent(category)

    if category.name.end_with?(" (#{category.id})")
      category.name.slice!(" (#{category.id})")
      category.save
    end

    if !category.name.end_with?(" (##{category.id})") && "private" == category.custom_fields["archetype"]
      category.update(name: category.name + " (##{category.id})") 
    end

  end

  # add_model_callback(GroupUser, :after_save) do
  #   puts "***************************************"
  #   puts "******** GroupUser after_save *********"
  #   puts "***************************************"
  #   # puts self.inspect
  # end

  # DiscourseEvent.on(:group_updated) do |group|
  #   puts "***************************************"
  #   puts "EVENT GROUP_UPDATED CALLED for #{group.id}"
  #   puts "***************************************"
  # end
  Category.register_custom_field_type("parent_category", :object)
  Category.register_custom_field_type("archetype", :string)
  # Category.register_custom_field_type("owner", :string)
  Category.register_custom_field_type("member_group_id", :string)
  if Site.respond_to? :preloaded_category_custom_fields 
    Site.preloaded_category_custom_fields << 'archetype'
    Site.preloaded_category_custom_fields << 'owner'
    Site.preloaded_category_custom_fields << 'member_group_id'
    Site.preloaded_category_custom_fields << 'parent_category'
  end
  Group.register_custom_field_type("category_id", :string)
  Site.preloaded_custom_fields << 'category_id' if Site.respond_to? :preloaded_custom_fields 

  add_to_serializer :basic_category, :archetype do
    object.custom_fields["archetype"]
  end

  add_to_serializer :basic_category, :parent_category do
    Category.find_by(id: object.parent_category_id)
  end
  add_to_serializer :basic_group, :category do
    Category.find_by(id: object.custom_fields["category_id"])
  end
  add_to_serializer(:basic_category, :archetype) { object.custom_fields["archetype"] }
  add_to_serializer :basic_category, :group do
    Group.find_by(id: object.custom_fields["member_group_id"])
  end

  reloadable_patch do

    class ::User
      module CheckUserOwnership
        def owns_category?(category)
          return category && category.reviewable_by_group && category.reviewable_by_group.users && category.reviewable_by_group.users.include?(self)
        end
      end
      prepend CheckUserOwnership
    end 

  end # reloadable_patch

end

def add_groups_to_parent(category)
  puts "add_groups_to_parentadd_groups_to_parentadd_groups_to_parent"
  if category.parent_category && category.parent_category.custom_fields["archetype"]=="privateparent"
    parent = category.parent_category
    puts parent.groups.pluck(:name)
    category.groups.each do |group|
      puts "CHECKING #{group.name}"
      unless parent.groups.include?(group)
        puts "ADDING"
        parent.groups << group
      end
      # parent.save
    end
  end
end

def create_groups(category)
  # Members group
  group = category.groups.create({
    "name"=>"C#{category.id}M", 
    "mentionable_level"=>"3", 
    "messageable_level"=>"99", 
    "visibility_level"=>"2", 
    "members_visibility_level"=>"2", 
    "automatic_membership_email_domains"=>"", 
    "title"=>"", 
    "primary_group"=>"false", 
    "flair_icon"=>"", 
    "flair_upload_id"=>"", 
    "flair_bg_color"=>"", 
    "flair_color"=>"", 
    "public_exit"=>"true", 
    "full_name"=>"#{category.name} - Members", 
    "publish_read_state"=>"true", 
    "muted_category_ids"=>["-1"], 
    "regular_category_ids"=>["-1"], 
    "watching_category_ids"=>["-1"], 
    "tracking_category_ids"=>["-1"], 
    "watching_first_post_category_ids"=>["-1"], 
    "usernames"=>""}
  )
  user_id = category.user_id
  # owner = User.find_by(username: category.custom_fields["owner"])
  # user_id = owner.id if owner

  group.group_users.build(user_id: user_id, owner: true)
  group.custom_fields["category_id"]=category.id
  if group.save
    group.restore_user_count!
  end
  category.custom_fields["member_group_id"]=group.id

  # Moderators group
  group2 = category.groups.create({
    "name"=>"C#{category.id}O", 
    "mentionable_level"=>"3", 
    "messageable_level"=>"4", 
    "visibility_level"=>"2", 
    "members_visibility_level"=>"2", 
    "automatic_membership_email_domains"=>"", 
    "title"=>"", 
    "primary_group"=>"false", 
    "flair_icon"=>"", 
    "flair_upload_id"=>"", 
    "flair_bg_color"=>"", 
    "flair_color"=>"", 
    "public_exit"=>"false", 
    "full_name"=>"#{category.name} - Moderators", 
    "publish_read_state"=>"false", 
    "muted_category_ids"=>["-1"], 
    "regular_category_ids"=>["-1"], 
    "watching_category_ids"=>["-1"], 
    "tracking_category_ids"=>["-1"], 
    "watching_first_post_category_ids"=>["-1"], 
    "usernames"=>"",
    "grant_trust_level"=>"0"
  })

  group2.group_users.build(user_id: user_id, owner: true)
  if group2.save
    group2.restore_user_count!
  end
  category.reviewable_by_group_id = group2.id

  puts "@@@@@@@@@@@@@@@@@@@@@@@ A"
  # Add groups to parent if private
  if category.parent_category
    puts "@@@@@@@@@@@@@@@@@@@@@@@ B"
    parent = category.parent_category
    if category.parent_category.custom_fields["archetype"]=="privateparent"
      puts "@@@@@@@@@@@@@@@@@@@@@@@ C"
      parent.groups << group
      parent.groups << group2
    end
    # parent.save
  end

  category.save

  # Remove admins
  admins = Group.find(1)
  category.groups.delete(admins)

end

def update_welcome_topic_owner(category)
  category.topics.first.update!(user: category.user, last_post_user_id: category.user.id)
  category.topics.first.posts.update_all(user_id: category.user.id, last_editor_id: category.user.id)
end

def create_about_topic(category)
  topic = Topic.find_by(category_id: category.id, archetype: "about");
  if topic then
    puts "ABOUT TOPIC FOUND. NOT DOING ANYTHING."
  else
    puts "NO ABOUT TOPIC FOUND. CREATING."
    about_topic_template = I18n.t("site_settings.category.about_topic_post_template", about_topic_replace_paragraph: I18n.t("site_settings.category.about_topic_replace_paragraph"))

    user = category.user
    # owner = User.find_by(username: category.custom_fields["owner"])
    # user = owner if owner
  
    Topic.transaction do
      t = Topic.new(title: I18n.t("site_settings.category.about_topic_prefix", category: category.name), user: user, pinned_at: Time.now, category_id: category.id)
      t.visible = false
      t.skip_callbacks = true
      t.ignore_category_auto_close = true
      t.delete_topic_timer(TopicTimer.types[:close])
      t.archetype = "about"
      t.save!(validate: false)
      post = t.posts.build(raw: category.description || about_topic_template, user: user)
      post.save!(validate: false)
      category.update_column(:description, post.cooked) if category.description.present?
      category.update(topic_id: t.id, 
                      subcategory_list_style: 'boxes_with_featured_topics',
                      show_subcategory_list: true,
                      topic_count: 1)
    end
  end
end